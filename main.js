// problem1
let x = 5;
console.log(x);
console.log(x + 2);
console.log(x - 6);
console.log(x * 30);
console.log(x / 2);
console.log(x % 5);
console.log(x ** 3);
// 1. add 2
// 2. substract 6
// 3. multiply by 30
// 4. divide by 2
// 5. modulo 5
// 6. exponent by 3

//problem 3
/*
1. does 0.1 + 0.2 equals to 0.3?
2. does true (as a boolean) equals to "true"?
3. does 1 + 7 not equals to 8?
4. is 10 in power 5 greater than 9 in power 6?
5. is 17 divided 6 less than or equal to 4?
*/
const number = 1 + 7;
const x1 = 0.1 + 0.2;
console.log(x1);
console.log(x1 == 0.3);
console.log(0.1 + 0.2 == 0.3);
console.log(number == 8);
console.log(1 + 7 == 8);
console.log(17 / 6 <= 4);
console.log(true == true);

// problem 4
let firstName = "John";
let price = 35;
let age = 18;

if (age < 18) {
  console.log("price should be free");
}
if (firstName.toLocaleLowerCase() == "John" || age == 25) {
  console.log("you have 20% discount");
} else {
  console.log("price will be 35 ");
}
